﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace MelhorRota
{
    public class Processar
    {
        /// <summary>
        /// Escalas em memória do arquivo de carga.
        /// </summary>
        private static List<Rotas> listRotasDoArquivo = new List<Rotas>();
        /// <summary>
        /// Objeto contendo todo plano de viagem possivel para rota desejada pelo cliente, ##OBS## este objeto poderia ser salvo
        /// em json para melhor performance e apenas ser atualizado quando valor/rota deste objeto for atualizado.
        /// </summary>
        private static PlanosDeViagem planoDeViagem = new PlanosDeViagem();


        /// <summary>
        /// Processar se rotas desenhadas existem nas rotas da empresa.
        /// </summary>
        /// <param name="rotaDesejada">Rota inputada pelo cliente padrão "DE-PARA"</param>
        /// <returns></returns>
        public bool ValidarProcessar(string rotaDesejada)
        {
            try
            {
                string[] arrayDePara = rotaDesejada.Split('-');
                //carrega origem desejada pelo cliente.
                planoDeViagem.Origem = arrayDePara[0].ToString();

                //carrega destino desejada pelo cliente.
                planoDeViagem.Destino = arrayDePara[1].ToString();

                //validar se Origem existe
                List<Rotas> rotasOrigem = (from r in listRotasDoArquivo
                                           where r.Origem == planoDeViagem.Origem
                                           select r).ToList();
                
                if (rotasOrigem.Count() == 0)
                {
                    ExibirMensagem("Desculpe não temos partidas de (" + arrayDePara[0] + ")");
                    return false;
                }

                //validar se destino existe
                List<Rotas> rotasDestino = (from r in listRotasDoArquivo
                                            where r.Destino == planoDeViagem.Destino
                                            select r).ToList();

                if (rotasDestino.Count() == 0)
                {
                    ExibirMensagem("Desculpe não temos destinos para (" + arrayDePara[1] + ")");
                    return false;
                }

                //Processar viagem de acordo com origem e destido informado pelo cliente.
                ProcessarViagen(planoDeViagem.Origem, planoDeViagem.Destino);
            }
            catch (Exception ex)
            {
                FinalizarComErro(ex, "Rotas invalidas (" + rotaDesejada + ")");
            }

            return true;
        }

        /// <summary>
        /// Processa todas as rotas posiveis para o viagem desejada.
        /// </summary>
        /// <param name="origemCliente"></param>
        /// <param name="destinoCliente"></param>
        public void ProcessarViagen(string origemCliente, string destinoCliente)
        {                    
            //Objeto que vai conter todas rotas de viagem
            List<RotasDeViagem> listRotasDeViagem = new List<RotasDeViagem>();

            //Carregar objeto com todas as rotas da empresa com origem = origem do cliente.
            foreach (Rotas item in listRotasDoArquivo)
            {
                if (item.Origem == origemCliente)
                {
                    RotasDeViagem obj = new RotasDeViagem();
                    obj.Escalas.Add(item);
                    listRotasDeViagem.Add(obj);
                }
               
            }

            //variável para controlar se todas as rotas foram processadas ate o destino do cliente o destino que não possui mais escala para outros lugares.
            bool finalizada = false;
            while (!finalizada)
            {
                try
                {
                    //Iniciar processamento das rotas.
                    foreach (RotasDeViagem item in listRotasDeViagem)
                    {
                        //Verificas se rota não esta finalizada e tambem não é ainda o destino do cliente.
                        if (item.Finalizada == false && item.Escalas.Last().Destino != destinoCliente)
                        {
                            //Pega proxima rota possivel.
                            List<Rotas> rotasDestino = (from r in listRotasDoArquivo
                                                        where r.Origem == item.Escalas.Last().Destino
                                                        select r).ToList();

                            //Se tiver mais de 1 quer dizer que esta rota possui mais destinos para processar ou seja esta escala pode ir para N destinos.
                            if (rotasDestino.Count > 1)
                            {
                                //Neste caso criamos um novo objeto para salvar a escala atual, pois esta será copiada para a próximo destino.
                                RotasDeViagem rotasDeViagem = new RotasDeViagem();
                                foreach (Rotas obj in item.Escalas)
                                {
                                    rotasDeViagem.Escalas.Add(obj);
                                }

                                //Processar todos o destinos que esta escala possui.
                                for (int i = 0; i < rotasDestino.Count(); i++)
                                {
                                    //Adiciona no RotasDeViagem atual.
                                    if (i == 0)
                                    {
                                        item.Escalas.Add(rotasDestino[i]);
                                    }
                                    else
                                    {
                                        //recriar objeto de escala ate onde ele encotrou n rotas de destino.
                                        RotasDeViagem rotasDeViagem2 = new RotasDeViagem();
                                        foreach (Rotas obj2 in rotasDeViagem.Escalas)
                                        {
                                            rotasDeViagem2.Escalas.Add(obj2);
                                        }

                                        rotasDeViagem2.Escalas.Add(rotasDestino[i]);
                                        listRotasDeViagem.Add(rotasDeViagem2);
                                    }
                                }

                            }
                            else if (rotasDestino.Count == 0)
                            {
                                //sinalizar que esta roda esta finalizada.
                                item.Finalizada = true;
                                item.Mensagem = "Rota final (" + item.Escalas.Last().Destino + ") não possui novas rotas";
                            }
                            else
                            {
                                //Esta rota ainda ainda pode ter outros destinos.
                                item.Escalas.Add(rotasDestino[0]);
                            }
                        }
                        else

                            //sinalizar que esta roda esta finalizada.
                            item.Finalizada = true;
                    }               

                }
                catch (Exception)
                {
                    //Editado objeto do foreach para nova rota encontrada.                    
                }

                //Testar se todas rotas foram processada ate o status final
                List<bool> teste = (from r in listRotasDeViagem
                                        where r.Finalizada == false
                                        select r.Finalizada).ToList();

                if (teste.Count == 0)
                {
                    finalizada = true;
                }
            }

            //carrega origem desejada pelo cliente.
            planoDeViagem.Origem = origemCliente;

            //carrega destino desejada pelo cliente.
            planoDeViagem.Destino = destinoCliente;

            //ira controlar todas a viagens possiveis com origem e destino  selecionado pelo cliente.
            List<RotasDeViagem> viagens = new List<RotasDeViagem>();
            foreach (RotasDeViagem item in listRotasDeViagem)
            {
                item.ValorTotalViagem = item.Escalas.Sum(x => x.Valor);
                viagens.Add(item);
            }

            planoDeViagem.Viagens = viagens;

        }

        /// <summary>
        /// Exibir a rota mais em conta para a viagem desejada.
        /// </summary>
        /// <returns></returns>
        public string MelhorRota()
        {
            //Toda rotas possiveis com origem o destino = cliente.
            List<RotasDeViagem> viagens = planoDeViagem.Viagens;

            //Carregar apenas rotas com destino = ao do cliente.
            List<RotasDeViagem> viagensDestinoCliente = new List<RotasDeViagem>();
            foreach (RotasDeViagem item in viagens)
            {
                foreach (Rotas item2 in item.Escalas)
                {
                    //se igual ao destino do cliente carrega para processar menor valor.
                    if (item2.Destino == planoDeViagem.Destino)
                    {
                        item.Mensagem = ">>>>>>>>>>>>>>> OPACAO DE VIAGEM PARA CLIENTE <<<<<<<<<<<<<<";
                        viagensDestinoCliente.Add(item);                        
                        break;
                    }   
                }
            }

            //Testar se existe ao menos uma rota para o destino do cliente
            if (viagensDestinoCliente.Count() == 0)
            {
                return "Desculpe não possuimos rotas com origem " + planoDeViagem.Origem + " até " + planoDeViagem.Destino + ".";
            }

            //conforme desejado será utilizado a rota mais barada para viagem.
            RotasDeViagem rotaMaisBarata = viagensDestinoCliente.OrderBy(v => v.ValorTotalViagem).First();

            //exibir conforme layout desejado melhor rota de viagem            
            StringBuilder rota = new StringBuilder();
            foreach (var item in rotaMaisBarata.Escalas)
            {
                rota.AppendFormat(" - {0}", item.Destino);
            }

            StringBuilder printFull = new StringBuilder();
            foreach (var item in viagens)
            {
                StringBuilder rotasConhecidas = new StringBuilder();
                foreach (var obj in item.Escalas)
                {
                    rotasConhecidas.AppendFormat(" escala [{0} (${1}) {2}]", obj.Origem, obj.Valor, obj.Destino);
                }
                printFull.AppendLine("Viagens >>> " + rotasConhecidas.ToString() + " Mensagem: " + item.Mensagem);

            }

            //exibir conforme layout desejado.            
            printFull.AppendLine("\nbest route: " + planoDeViagem.Origem + rota + " > $" + rotaMaisBarata.ValorTotalViagem);

            return printFull.ToString();
        }

        /// <summary>
        /// Processar arquivo de carga de rotas.
        /// </summary>
        /// <param name="path">Path completo do arquivo de rotas que deseja carregar na aplicação</param>
        public void ProcessarArquivo(string path)
        {
            try
            {
                String[] array;
                TextFieldParser CSVFile1 = new TextFieldParser(path);
                CSVFile1.SetDelimiters(",");

                while (!CSVFile1.EndOfData)
                {
                    array = CSVFile1.ReadFields();

                    Rotas rota = new Rotas();
                    rota.Origem = array[0].ToString();
                    rota.Destino = array[1].ToString();
                    rota.Valor = float.Parse(array[2].ToString());

                    listRotasDoArquivo.Add(rota);
                }
            }
            catch (Exception ex)
            {
                FinalizarComErro(ex, "Ocorreu um erro na carga do seu arquivo de rotas, verificar arquivo.");
            }
        }


        /// <summary>
        /// Exibe mensagem no console.
        /// </summary>
        /// <param name="msg">Mensagem para exibição</param>
        public void ExibirMensagem(string msg)
        {
            Console.WriteLine(msg);
        }

        /// <summary>
        /// Finaliza aplicação
        /// </summary>
        private void Finalizar()
        {
            Environment.Exit(-1);
        }

        /// <summary>
        /// Finalizar app com erro.
        /// </summary>
        /// <param name="msgException"></param>
        private void FinalizarComErro(Exception ex, string msg)
        {
            ExibirMensagem("======================================");
            ExibirMensagem(msg + "\n" + ex.Message);
            ExibirMensagem("======================================");
            Finalizar();
        }
    }
}
