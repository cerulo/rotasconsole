﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MelhorRota
{
    public class RotasDeViagem
    {
        /// <summary>
        /// Sequencia de escalas desta viagem.
        /// </summary>
        public List<Rotas> Escalas { get; set; }
   
        /// <summary>
        /// Indica que esta rota já foi processada ate sua ultima escala possivel ou seja = ao destino desejado do cliente
        /// </summary>
        public bool Finalizada { get; set; }
        /// <summary>
        /// Objeto de mensagem relevante para esta rota de viagem.
        /// </summary>
        public string Mensagem { get; set; }
        /// <summary>
        /// Objeto com valor total desta viagem, utilizado para filtrar viagem mais barada ta cliente.
        /// </summary>
        public float ValorTotalViagem { get; set; }

        public RotasDeViagem()
        {
            Escalas = new List<Rotas>();
            Finalizada = false;
            Mensagem = string.Empty;
            ValorTotalViagem = 0;
        }

        public List<Rotas> GetEscalas()
        {
            return Escalas;
        }

    }
}
