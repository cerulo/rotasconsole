﻿using MelhorRota;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Text;

namespace BestRoute
{
    class Program
    {
        static void Main(string[] args)
        {
            Processar processar = new Processar();

            //string path = @"C:\Users\cerul\Downloads\test\input-file.csv";
            string path = args[0].ToString();

            if (path.Length == 0)
            {
                processar.ExibirMensagem("Favor inserir o path do arquivo (.csv) de rotas.");
            }
            else
            {
                //carga do arquivo selecionado pelo usuario.
                processar.ProcessarArquivo(path);
                
                while (true)
                {                   
                    Console.Write("please enter the route: ");
                    string rota = Console.ReadLine();

                    //Liberar processamento se padrão de insert estiver correto.
                    if(rota.Contains('-'))
                    {
                        //Valida se rotas existem e processa rotas de viagens.
                        if (processar.ValidarProcessar(rota))
                        {
                            //Existe resultados.
                            processar.ExibirMensagem(processar.MelhorRota());
                        }
                    }
                }
            }
        }
    }
}
