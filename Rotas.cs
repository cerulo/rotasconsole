﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MelhorRota
{
    /// <summary>
    /// Rotas da empresa.
    /// </summary>
    public class Rotas
    {
        /// <summary>
        /// Origem desta Rota.
        /// </summary>
        public string Origem { get; set; }

        /// <summary>
        /// Destino desta Rota.
        /// </summary>
        public string Destino { get; set; }

        /// <summary>
        /// Valor desta Rota, utilizado para processar o total da viagem desejada pelo cliente.
        /// </summary>
        public float Valor { get; set; }
    }
}
